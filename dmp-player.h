/*
 * Copyright (c) 2015 Víctor Jáquez <vjaquez@igalia.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Control Center; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#pragma once

#include <gst/player/player.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define DMP_TYPE_PLAYER (dmp_player_get_type ())
G_DECLARE_FINAL_TYPE (DmpPlayer, dmp_player, DMP, PLAYER, GtkApplicationWindow)

DmpPlayer* dmp_player_new (void);
GstPlayer* dmp_player_get_player (DmpPlayer * self);

void dmp_player_open (DmpPlayer * self, const gchar * uri);
void dmp_player_play (DmpPlayer * self);
void dmp_player_stop (DmpPlayer * self);
void dmp_player_pause (DmpPlayer * self);
void dmp_player_set_position (DmpPlayer * self, GstClockTime position);
void dmp_player_seek (DmpPlayer * self, gint64 offset);
void dmp_player_set_volume (DmpPlayer * self, gdouble volume);
gdouble dmp_player_get_volume (DmpPlayer * self);
gchar *dmp_player_set_next_subtitle (DmpPlayer * self);

G_END_DECLS
