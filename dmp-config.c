/*
 * Copyright (c) 2014 Endless Mobile, Inc.
 * Copyright (c) 2017 Víctor Jáquez <vjaquez@igalia.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Control Center; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "dmp-config.h"

#include <gio/gio.h>
#include <string.h>

struct _DmpConfig
{
#define GETTERS(p) gchar *p;
  PARAMS_LIST(GETTERS)
#undef GETTERS
  guint timeout;
};

static gpointer
dmp_config_init (gpointer data)
{
  DmpConfig *cfg = g_slice_new0 (DmpConfig);
  dmp_config_load (cfg, NULL);
  return cfg;
}

DmpConfig *
dmp_config_get (void)
{
  static GOnce once_init = G_ONCE_INIT;
  return g_once (&once_init, dmp_config_init, NULL);
}

void
dmp_config_set (DmpConfig *cfg,
#define SET_PARAMS(p) gchar *p,
    PARAMS_LIST(SET_PARAMS)
#undef SET_PARAMS
    gpointer end)
{
  if (!cfg)
    cfg = dmp_config_get ();

  if (!cfg)
    return;

#define SETTERS(p) if (p) { g_free (cfg->p); cfg->p = p; }
  PARAMS_LIST(SETTERS)
#undef SETTERS
}

void
dmp_config_free (DmpConfig *cfg)
{
  if (!cfg)
    cfg = dmp_config_get ();

  if (!cfg)
    return;

#define CLEARERS(p) g_clear_pointer (&cfg->p, g_free);
  PARAMS_LIST(CLEARERS)
#undef CLEARERS
}

void
dmp_config_destroy (DmpConfig *cfg)
{
  if (!cfg)
    cfg = dmp_config_get ();

  if (!cfg)
    return;

  dmp_config_free (cfg);
  g_slice_free (DmpConfig, cfg);
}

static inline gchar *
get_str (GKeyFile *keyfile, gchar *grp, gchar *key)
{
  GError *err = NULL;
  gchar *value = g_key_file_get_string (keyfile, grp, key, &err);
  if (err) {
    g_free (value);
    g_error_free (err);
    return NULL;
  }

  return g_strstrip (value);
}

static GKeyFile *
load_default (void)
{
  GKeyFile *keyfile;
  gchar *fname;
  gboolean loaded;
  GError *err = NULL;

  keyfile = g_key_file_new ();
  fname = g_build_filename (g_get_user_config_dir (), "dplayer", "config", NULL);
  loaded = g_key_file_load_from_file (keyfile, fname, G_KEY_FILE_NONE, &err);
  g_free (fname);

  if (!loaded) {
    g_info ("failed to load config file: %s", err->message);
    g_error_free (err);
    g_key_file_unref (keyfile);
    return NULL;
  }

  return keyfile;
}

gboolean
dmp_config_load (DmpConfig *cfg, GKeyFile *keyfile)
{
  g_return_val_if_fail (cfg, FALSE);

  gboolean ret = FALSE;
  gboolean own = FALSE;
  gchar *grp;
#define V(p) gchar *p;
    PARAMS_LIST(V)
#undef V

  if (!keyfile) {
    if (!(keyfile = load_default ()))
      return FALSE;
    own = TRUE;
  }

  grp = g_key_file_get_start_group (keyfile);
#define LOAD_PARAM(p) p = get_str (keyfile, grp, G_STRINGIFY (p));
    PARAMS_LIST(LOAD_PARAM)
#undef LOAD_PARAM

  dmp_config_set (cfg,
#define PARAM(p) p,
    PARAMS_LIST(PARAM)
#undef PARAM
    NULL);

  ret = TRUE;

  g_free (grp);

  if (own)
    g_key_file_unref (keyfile);

  return ret;
}

#define GETTERS(p) \
  const gchar *dmp_config_get_##p () { return dmp_config_get ()->p; }
PARAMS_LIST(GETTERS)
#undef GETTERS

static gint
strcmp_sized (const gchar *s1, size_t len1, const gchar *s2)
{
  size_t len2 = strlen (s2);
  return strncmp (s1, s2, MAX (len1, len2));
}

gint
str2bool (const gchar  *value)
{
  gint i, length = 0;

  if (!value || value[0] == '\0')
    return -1;

  /* Count the number of non-whitespace characters */
  for (i = 0; value[i]; i++)
    if (!g_ascii_isspace (value[i]))
      length = i + 1;

  if (strcmp_sized (value, length, "true") == 0
      || strcmp_sized (value, length, "1") == 0
      || strcmp_sized (value, length, "yes") == 0)
    return TRUE;
  else if (strcmp_sized (value, length, "false") == 0
           || strcmp_sized (value, length, "0") == 0
           || strcmp_sized (value, length, "no") == 0)
    return FALSE;

  return -1;
}
