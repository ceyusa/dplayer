#!/bin/sh

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

(test -f $srcdir/configure.ac) || {
        echo "**Error**: Directory "\`$srcdir\'" does not look like the top-level project directory"
        exit 1
}

set -x
mkdir m4
autoreconf --verbose --force --install -Wno-portability || exit 1
{ set +x; } 2>/dev/null

if [ "$NOCONFIGURE" = "" ]; then
    set -x
    $srcdir/configure "$@" || exit 1
    { set +x; } 2>/dev/null
fi
