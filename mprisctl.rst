mprisctl
########

###########################
Control a MPRIS application
###########################

:Copyright: GPLv2+
:Manual section: 1
:Manual group: multimedia

SYNOPSIS
========

| **mprisctl** [OPTIONS…] COMMAND [ARGS…]

DESCRIPTION
===========

**mprisctl** may be used to control the state of any MPRIS application
D-bus service.

OPTIONS
=======

The following options are understood:

``-p, --player=…``
    Set the name of the player to control. By the default it will choose
    **dplayer**.

    .. admonition:: Example

       ``mprisctl --player=spotify play``
           Would play the current song in Spotify.

``-l, --list-all``
    List all the available MPRIS players. Afterwards it exists.

COMMANDS
========

The following commands are understood:

**Playing commands**

``open <URI>``
    **URI** is the URI of the track to load.

    Opens the URI given as an argument.

    If the playback is stopped, starts playing.

``play``
    Starts or resumes playback.

    If already playing, this has no effect.

    If paused, playback resumes from the current position.

    If there is no track to play, this has no effect.

``pause``
    Pauses playback.

    If playback is already paused, this has no effect.

``play-pause``
    Pauses playback.

    If playback is already paused, resumes playback.

    If playback is stopped, starts playback.

``stop``
    Stops playback.

``next``
    Skips to the next track in the tracklist.

``previous``
    Skips to the previous track in the tracklist.

``position <POS|SEEK[+|-]>``
    ``POS`` is an absolute position of the media file in seconds. Must
    be between 0 and <track_length>.

    ``SEEK`` is the relative position of the media file in seconds,
    according the current one. If a ``+`` is appended, the position is
    forward the current position; if a ``-`` is appended, is backwards.

    If no argument is passed, the current position in seconds is
    printed.

``volume <VOLUME>``
    ``VOLUME`` is a float value between [0..1]. It sets the current
    volume level.

    If no argument is passed, the current volume level is printed.

``status``
    Prints the current playback status.

    It may be **Playing**, **Paused** or **Stopped**.

``metadata``
    Prints the metadata of the current element.

``shuffle <Y|N>``
    A value of ``N`` indicates that playback is progressing linearly
    through a playlist, while ``Y`` means playback is progressing
    through a playlist in some other order.

    If no argument is passed, prints the current shuffle mode.

``rate <MIN_RATE..MAX_RATE>``
    Sets the current playback rate. The value must fall in the range
    described by MIN_RATE and MAX_RATE, and must not be 0.0.

    If no argument is passed, prints the current rate.

``loop <None|Track|Playlist>``
    Sets or prints the current loop / repeat status.

    **None**
        if the playback will stop when there are no more tracks to play.
    **Track**
        if the current track will start again from the beginning once it
        has finished playing.
    **Playlist**
        if the playback loops through a list of tracks.


**Application commands**

``quit``
    Causes the media player to stop running. The player can refuse to do
    it.

``raise``
    Brings the media player's user interface to the front using any
    appropriate mechanism available. The player may ignore it.

``fs``
    Flips the fullscreen mode of the application.
