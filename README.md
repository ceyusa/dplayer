# DBUS media player

DPlayer a media player, based on
[gtk-player](https://cgit.freedesktop.org/gstreamer/gst-examples/tree/playback/player/gtk),
controlled only by D-Bus using [MPRIS
interface](http://www.freedesktop.org/wiki/Specifications/mpris-spec/).

## D-bus service installation

After build the sources, you will need to setup a D-bus activation environment.

In the sources there is a shell script named `dplayer-wrapper`. Its purpose is
to execute `dplayer` under a
[jhbuild](https://cgit.freedesktop.org/gstreamer/gst-examples/tree/playback/player/gtk)
environment, but you can change it for you preferred compilation environment
such as
[gst-uninstalled](https://arunraghavan.net/2014/07/quick-start-guide-to-gst-uninstalled-1-x/)
or [gst-build](https://cgit.freedesktop.org/gstreamer/gst-build/tree/README.md)

Also, in the sources there is a D-bus service file named
`org.mpris.MediaPlayer2.dplayer.service`. You should modify it to execute your
`dplayer-wrapper` under your user. For example:

```
[D-BUS Service]
Name=org.mpris.MediaPlayer2.dplayer
Exec=<path to>/dplayer-wrapper
User=<username>
```

Then copy this file into your personal D-bus services:
`~/.local/share/dbus-1/services/`, et voilà, D-bus should be able to find and
activate `dplayer` when you request for it. For example:

```
$ gdbus introspect --session --recurse --dest org.mpris.MediaPlayer2.dplayer \
                   --object-path /org/mpris/MediaPlayer2
```

## D-bus MPRIS client

Also it is included a command line program to control any MPRIS service called
`mprisctl`. You can read its [manual
file](https://gitlab.com/ceyusa/dplayer/blob/master/mprisctl.rst).

For example:

```
$ mprisctl open Videos/movie.avi
$ mprisctl position 60+           # seek 1 minute forward
```
