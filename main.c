/*
 * Copyright (c) 2017 Víctor Jáquez <vjaquez@igalia.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Control Center; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "dmp-service.h"

#include <gst/gst.h>

#if defined (GDK_WINDOWING_X11)
#include <gdk/gdkx.h>
#endif

int
main (int argc, char **argv)
{
  int retval;
#if defined (GDK_WINDOWING_X11)
  XInitThreads ();
#endif

  g_autoptr (DmpService) srv = dmp_service_new ();
  retval = g_application_run (G_APPLICATION (srv), argc, argv);
  gst_deinit ();

  return retval;
}
