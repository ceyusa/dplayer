/*
 * Copyright (c) 2015 Víctor Jáquez <vjaquez@igalia.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Control Center; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#pragma once

#include "dmp-service.h"

#include <gst/player/player.h>

G_BEGIN_DECLS

typedef struct _DmpMediaInfo DmpMediaInfo;
struct _DmpMediaInfo
{
  GstPlayerMediaInfo *info;
  const gchar *title;
  const gchar *subtitle_id;
  const gchar *uri;
};

void dmp_service_player_set_media_info (DmpService *service,
                                        DmpMediaInfo *info);

void dmp_service_player_set_state (DmpService *service,
                                   GstPlayerState state);

guint64 dmp_service_player_get_position (DmpService *service);

gdouble dmp_service_player_get_volume (DmpService *service);

void dmp_service_player_set_volume (DmpService *service, gdouble volume);

gdouble dmp_service_player_get_rate (DmpService *service);

void dmp_service_player_set_rate (DmpService *service, gdouble rate);

void dmp_service_player_emit_seeked (DmpService *service, gint64 position);

void dmp_service_player_quit (DmpService *service);

G_END_DECLS
