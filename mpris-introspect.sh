#!/bin/bash

GDBUS=`which gdbus`

DEST="org.mpris.MediaPlayer2"
PATH="/org/mpris/MediaPlayer2"

ARG=${1};
player=${ARG:="dplayer"}

$GDBUS introspect --session --dest $DEST.${player} --object-path $PATH --recurse

