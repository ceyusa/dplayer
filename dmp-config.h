/*
 * Copyright (c) 2017 Víctor Jáquez <vjaquez@igalia.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Control Center; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

typedef struct _DmpConfig DmpConfig;

DmpConfig      *dmp_config_get                  (void);

void            dmp_config_free                 (DmpConfig *cfg);

void            dmp_config_destroy              (DmpConfig *cfg);

gboolean        dmp_config_load                 (DmpConfig *cfg,
                                                 GKeyFile *keyfile);

void            dmp_config_dump                 (DmpConfig *cfg);

gint            str2bool                        (const gchar  *value);

#define PARAMS_LIST(V)  \
  V(subtitle_font_desc) \
  V(video_output)       \
  V(watch_later)        \
  V(ytdl_path)

void            dmp_config_set (DmpConfig *cfg,
#define SET_PARAMS(p) gchar *p,
  PARAMS_LIST(SET_PARAMS)
#undef SET_PARAMS
                                gpointer end);


#define GETTERS(p) const gchar *dmp_config_get_##p (void);
PARAMS_LIST(GETTERS)
#undef GETTERS

G_END_DECLS
