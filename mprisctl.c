/*
 * Copyright (c) 2017 Víctor Jáquez <vjaquez@igalia.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Control Center; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <errno.h>
#include <gio/gio.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>

#include "org.mpris.MediaPlayer2.h"
#include "org.mpris.MediaPlayer2.Player.h"

static gchar *opt_player_name;
static gchar **opt_command;
static gboolean opt_list;

#define COMMAND_LIST(V) \
  V(play)               \
  V(pause)              \
  V(play_pause)         \
  V(stop)               \
  V(next)               \
  V(previous)

#define dmp_media_player2_player_get_can_next   \
  dmp_media_player2_player_get_can_go_next
#define dmp_media_player2_player_get_can_previous       \
  dmp_media_player2_player_get_can_go_previous
#define dmp_media_player2_player_get_can_play_pause     \
  dmp_media_player2_player_get_can_pause
#define dmp_media_player2_player_get_can_stop   \
  dmp_media_player2_player_get_can_control

#define COMMAND_FUNC(cmd)                                               \
static gboolean                                                         \
cmd##_cmd (DmpMediaPlayer2Player * proxy, gchar ** args)                \
{                                                                       \
  GError *err = NULL;                                                   \
  gboolean ret;                                                         \
                                                                        \
  if (!dmp_media_player2_player_get_can_##cmd (proxy)) {                \
    g_print ("Player can't " G_STRINGIFY (cmd) "\n");                   \
    return TRUE;                                                        \
  }                                                                     \
  ret = dmp_media_player2_player_call_##cmd##_sync (proxy, NULL, &err); \
  if (err) {                                                            \
    g_printerr ("D-bus error: %s\n", err->message);                     \
    g_error_free (err);                                                 \
  }                                                                     \
  return ret;                                                           \
}                               /* closes cmd##_cmd */

COMMAND_LIST(COMMAND_FUNC)
#undef COMMAND_FUNC

#define SECONDS_AS_MSECONDS(secs) ((secs) * G_GINT64_CONSTANT (1000000))
#define MSECONDS_AS_SECONDS(msecs) ((msecs) / G_GINT64_CONSTANT (1000000))

static inline void
dump_variant (GVariant * v)
{
  gchar *s;

  s = g_variant_print (v, FALSE);
  if (s) {
    g_print ("%s\n", s);
    g_free (s);
  }
}

static gboolean
position_cmd (DmpMediaPlayer2Player * proxy, gchar ** args)
{
  gchar *tail = NULL;
  gint64 pos;
  gboolean ret;
  GError *err = NULL;
  GVariant *metadata, *tid;
  const gchar *trackid;

  if (!args[1]) {
    pos = dmp_media_player2_player_get_position (proxy);
    g_print ("Current position (seconds): %g\n",
        (double) MSECONDS_AS_SECONDS (pos));
    return TRUE;
  }

  if (!dmp_media_player2_player_get_can_seek (proxy)) {
    g_print ("Player can't seek neither set position\n");
    return TRUE;
  }

  errno = 0;
  pos = strtol (args[1], &tail, 10);
  if (errno || (*tail != '\0' && *tail != '+' && *tail != '-')) {
    g_printerr ("Couldn't parse the requested position\n");
    return FALSE;
  }

  if (*tail == '+' || *tail == '-') {
    if (*tail == '-')
      pos *= -1;
    ret = dmp_media_player2_player_call_seek_sync (proxy,
        SECONDS_AS_MSECONDS (pos), NULL, &err);
    if (err) {
      g_printerr ("D-bus error: %s\n", err->message);
      g_error_free (err);
    }
    return ret;
  }

  metadata = dmp_media_player2_player_get_metadata (proxy);
  if (!metadata) {
    g_printerr ("Couldn't fetch track metadata\n");
    return FALSE;
  }

  tid = g_variant_lookup_value (metadata, "mpris:trackid",
      G_VARIANT_TYPE_OBJECT_PATH);
  if (!tid) {
    g_printerr ("Couldn't find track id to set position\n");
    return FALSE;
  }

  trackid = g_variant_get_string (tid, NULL);

  ret = dmp_media_player2_player_call_set_position_sync (proxy, trackid,
      SECONDS_AS_MSECONDS (pos), NULL, &err);

  g_variant_unref (tid);

  if (err) {
    g_printerr ("D-bus error: %s\n", err->message);
    g_error_free (err);
  }

  return ret;
}

static gboolean
metadata_cmd (DmpMediaPlayer2Player * proxy, gchar ** args)
{
  GVariant *metadata, *entry;
  GVariantIter iter;
  gboolean found;

  metadata = dmp_media_player2_player_get_metadata (proxy);
  if (!metadata) {
    g_printerr ("Couldn't fetch track's metadata\n");
    return FALSE;
  }

  if (!args[1]) {
    dump_variant (metadata);
    return TRUE;
  }

  found = FALSE;
  g_variant_iter_init (&iter, metadata);
  while ((entry = g_variant_iter_next_value (&iter))) {
    GVariant *key = g_variant_get_child_value (entry, 0);
    gboolean matches = g_str_has_suffix (g_variant_get_string (key, NULL), args[1]);
    g_variant_unref (key);

    if (matches) {
      found = TRUE;
      dump_variant (entry);
    }

    g_variant_unref (entry);
  }

  if (!found) {
    g_printerr ("Couldn't found metadata key\n");
    return FALSE;
  }

  return TRUE;
}

static gboolean
volume_cmd (DmpMediaPlayer2Player * proxy, gchar ** args)
{
  gdouble vol;
  gchar *tail = NULL;

  if (!args[1]) {
    vol = dmp_media_player2_player_get_volume (proxy);
    g_print ("Volumen: %g\n", vol);
    return TRUE;
  }

  if (!dmp_media_player2_player_get_can_control (proxy)) {
    g_print ("Player can't set volume\n");
    return TRUE;
  }

  errno = 0;
  vol = strtod (args[1], &tail);
  if (errno || (*tail != '\0' && !g_ascii_isspace (*tail))) {
    g_printerr ("Couldn't parse the requested volume\n");
    return FALSE;
  }

  if (vol < 0 || vol > 1) {
    g_printerr ("Invalid paramter: [0..1]\n");
    return FALSE;
  }

  dmp_media_player2_player_set_volume (proxy, vol);
  return TRUE;
}

static gboolean
status_cmd (DmpMediaPlayer2Player * proxy, gchar ** args)
{
  const gchar *status;

  status = dmp_media_player2_player_get_playback_status (proxy);
  if (!status) {
    g_printerr ("Couldn't fetch player status\n");
    return FALSE;
  }

  g_print ("Status: %s\n", status);
  return TRUE;
}

static gboolean
open_url_cmd (DmpMediaPlayer2Player * proxy, gchar ** args)
{
  gboolean ret;
  GError *err = NULL;

  if (!args[1]) {
    g_printerr ("Missing URL\n");
    return FALSE;
  }

  ret =
      dmp_media_player2_player_call_open_uri_sync (proxy, args[1], NULL, &err);

  if (err) {
    g_printerr ("D-bus error: %s\n", err->message);
    g_error_free (err);
  }

  return ret;
}

static gboolean
rate_cmd (DmpMediaPlayer2Player * proxy, gchar ** args)
{
  gdouble rate, min, max;
  gchar *tail;

  if (!args[1]) {
    rate = dmp_media_player2_player_get_rate (proxy);
    g_print ("Current playback rate: %g\n", rate);
    return TRUE;
  }

  errno = 0;
  rate = strtod (args[1], &tail);
  if (errno || (*tail != '\0' && !g_ascii_isspace (*tail))) {
    g_printerr ("Couldn't parse the requested rate\n");
    return FALSE;
  }

  min = dmp_media_player2_player_get_minimum_rate (proxy);
  max = dmp_media_player2_player_get_maximum_rate (proxy);

  if (rate == 0 || rate < min || rate > max) {
    g_printerr ("Invalid playback rate [%g..%g]: %g\n", min, max, rate);
    return FALSE;
  }

  dmp_media_player2_player_set_rate (proxy, rate);
  return TRUE;
}

static gboolean
shuffle_cmd (DmpMediaPlayer2Player * proxy, gchar ** args)
{
  gboolean shuffle;

  if (!args[1]) {
    shuffle = dmp_media_player2_player_get_shuffle (proxy);
    g_print ("Playback is progressing is %s\n",
        shuffle ? "shuffled" : "linearly");
    return TRUE;
  }

  if (!dmp_media_player2_player_get_can_control (proxy)) {
    g_print ("Player can't set shuffle\n");
    return TRUE;
  }

  if (*args[1] != 'Y' && *args[1] != 'y'
      && *args[1] != 'N' && *args[1] != 'n') {
    g_printerr ("Invalid parameter: [Y | N]\n");
    return FALSE;
  }

  shuffle = (*args[1] == 'Y' || *args[1] == 'y');
  dmp_media_player2_player_set_shuffle (proxy, shuffle);

  return TRUE;
}

static gboolean
loop_cmd (DmpMediaPlayer2Player * proxy, gchar ** args)
{
  gint i;
  const gchar *loop_status;
  const static gchar *loopstat[] = { "None", "Track", "Playlist" };

  if (!args[1]) {
    loop_status = dmp_media_player2_player_get_loop_status (proxy);
    g_print ("Current loop status is %s\n", loop_status);
    return TRUE;
  }

  if (!dmp_media_player2_player_get_can_control (proxy)) {
    g_print ("Player can't set shuffle\n");
    return TRUE;
  }

  for (i = 0; i < G_N_ELEMENTS (loopstat); i++) {
    if (g_strcmp0 (args[1], loopstat[i]) == 0) {
      dmp_media_player2_player_set_loop_status (proxy, args[1]);
      return TRUE;
    }
  }

  g_printerr ("Invalid parameter: [None | Track | Playlist]\n");

  return FALSE;
}

static gboolean
cycle_subs_cmd (DmpMediaPlayer2Player * proxy, gchar ** args)
{
  gboolean ret;
  gchar *sub_desc = NULL;
  GError *err = NULL;

  ret = dmp_media_player2_player_call_cycle_subtitles_sync (proxy, &sub_desc,
      NULL, &err);
  if (err) {
    g_printerr ("D-bus error: %s\n", err->message);
    g_error_free (err);
  } else if (sub_desc) {
    g_print ("Current subtitle: %s\n", sub_desc);
    g_free (sub_desc);
  }
  return ret;
}

struct _Command
{
  const gchar *name;
  gboolean (*func) (DmpMediaPlayer2Player * proxy, gchar ** args);
} CMDS[] = {
  { "play", play_cmd },
  { "pause", pause_cmd },
  { "play-pause", play_pause_cmd },
  { "stop", stop_cmd },
  { "next", next_cmd },
  { "previous", previous_cmd },
  { "position", position_cmd },
  { "volume", volume_cmd },
  { "status", status_cmd },
  { "metadata", metadata_cmd },
  { "open", open_url_cmd },
  { "shuffle", shuffle_cmd },
  { "rate", rate_cmd },
  { "loop", loop_cmd },
  { "cycle-subs", cycle_subs_cmd },
};

static gboolean
quit_cmd (DmpMediaPlayer2 * proxy, gchar ** args)
{
  GError *err = NULL;
  gboolean ret;

  if (!dmp_media_player2_get_can_quit (proxy)) {
    g_print ("Player can't quit\n");
    return TRUE;
  }

  ret = dmp_media_player2_call_quit_sync (proxy, NULL, &err);
  if (err) {
    g_printerr ("D-bus error: %s\n", err->message);
    g_error_free (err);
  }
  return ret;
}

static gboolean
raise_cmd (DmpMediaPlayer2 * proxy, gchar ** args)
{
  GError *err = NULL;
  gboolean ret;

  if (!dmp_media_player2_get_can_raise (proxy)) {
    g_print ("Player can't raise\n");
    return TRUE;
  }

  ret = dmp_media_player2_call_raise_sync (proxy, NULL, &err);
  if (err) {
    g_printerr ("D-bus error: %s\n", err->message);
    g_error_free (err);
  }
  return ret;
}

static gboolean
fs_cmd (DmpMediaPlayer2 * proxy, gchar ** args)
{
  gboolean fs;

  if (!dmp_media_player2_get_can_set_fullscreen (proxy)) {
    g_print ("Player can't enter in fullscreen mode\n");
    return TRUE;
  }

  fs = dmp_media_player2_get_fullscreen (proxy);
  dmp_media_player2_set_fullscreen (proxy, !fs);

  return TRUE;
}

struct _CommandApp
{
  const gchar *name;
  gboolean (*func) (DmpMediaPlayer2 * proxy, gchar ** args);
} CMDS_APP[] = {
  { "quit", quit_cmd },
  { "raise", raise_cmd },
  { "fs", fs_cmd },
};

static DmpMediaPlayer2Player *
get_player_proxy (const gchar * name)
{
  DmpMediaPlayer2Player *proxy;
  GError *err = NULL;

  proxy =
      dmp_media_player2_player_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
      G_DBUS_PROXY_FLAGS_NONE, name, "/org/mpris/MediaPlayer2", NULL, &err);

  if (!proxy) {
    g_printerr ("D-bus error: %s\n", err->message);
    g_error_free (err);
    return NULL;
  }

  return proxy;
}

static DmpMediaPlayer2 *
get_app_proxy (const gchar * name)
{
  DmpMediaPlayer2 *proxy;
  GError *err = NULL;

  proxy =
      dmp_media_player2_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
      G_DBUS_PROXY_FLAGS_NONE, name, "/org/mpris/MediaPlayer2", NULL, &err);

  if (!proxy) {
    g_printerr ("D-bus error: %s\n", err->message);
    g_error_free (err);
    return NULL;
  }

  return proxy;
}

static gboolean
process_player_command (const gchar *dbusname, struct _Command *cmd)
{
  DmpMediaPlayer2Player *proxy;
  gboolean ret;

  proxy = get_player_proxy (dbusname);
  if (!proxy)
    return FALSE;

  ret = cmd->func (proxy, opt_command);
  g_object_unref (proxy);

  return ret;
}

static gboolean
process_app_command (const gchar *dbusname, struct _CommandApp *cmd)
{
  DmpMediaPlayer2 *proxy;
  gboolean ret;

  proxy = get_app_proxy (dbusname);
  if (!proxy)
    return FALSE;

  ret = cmd->func (proxy, opt_command);
  g_object_unref (proxy);

  return ret;
}

static gboolean
process_command (gchar * dbusname)
{
  gint i, len;

  if (!dbusname)
    return FALSE;

  len = strlen (opt_command[0]);
  for (i = 0; i < G_N_ELEMENTS (CMDS); i++) {
    if ((g_ascii_strncasecmp (opt_command[0], CMDS[i].name, len) == 0)
        && CMDS[i].func) {
      return process_player_command (dbusname, &CMDS[i]);
    }
  }

  for (i = 0; i < G_N_ELEMENTS (CMDS_APP); i++) {
    if ((g_ascii_strncasecmp (opt_command[0], CMDS_APP[i].name, len) == 0)
        && CMDS_APP[i].func) {
      return process_app_command (dbusname, &CMDS_APP[i]);
    }
  }

  return FALSE;
}

static gchar *
get_dbus_name (gchar ** names)
{
  guint i, len;

  /* default and we expect autolaunching */
  if (!names || !opt_player_name
      || (g_strcmp0 (opt_player_name, "dplayer") == 0))
    return "org.mpris.MediaPlayer2.dplayer";

  /* let's look for the player_name */
  len = g_strv_length (names);
  for (i = 0; i < len; i++)
    if (g_str_has_suffix (names[i], opt_player_name))
      return names[i];

  return NULL;
}

static gchar **
get_player_names ()
{
  GDBusProxy *proxy;
  GVariant *reply, *child;
  gsize count;
  const gchar **names;
  int i, j;
  gchar **ret = NULL;
  GError *err = NULL;

  proxy = g_dbus_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
      G_DBUS_PROXY_FLAGS_NONE, NULL, "org.freedesktop.DBus",
      "/org/freedesktop/DBus", "org.freedesktop.DBus", NULL, &err);
  if (!proxy)
    goto error;

  reply = g_dbus_proxy_call_sync (proxy, "ListNames", NULL,
      G_DBUS_CALL_FLAGS_NONE, -1, NULL, &err);
  if (!reply) {
    g_object_unref (proxy);
    goto error;
  }

  child = g_variant_get_child_value (reply, 0);
  names = g_variant_get_strv (child, &count);

  j = 0;
  for (i = 0; i < count; i++) {
    if (g_str_has_prefix (names[i], "org.mpris.MediaPlayer2")) {
      ret = g_realloc (ret, j + 1);
      ret[j++] = g_strdup (names[i]);
    }
  }

  if (j > 0) {
    ret = g_realloc (ret, j + 1);
    ret[j++] = NULL;
  }

  g_free (names);
  g_variant_unref (child);
  g_variant_unref (reply);
  g_object_unref (proxy);

  return ret;

error:
  g_printerr ("D-bus error: %s\n", err->message);
  g_error_free (err);

  return NULL;
}

static void
print_player_names (gchar ** names)
{
  guint i, l, plen;

  l = g_strv_length (names);
  plen = strlen ("org.mpris.MediaPlayer2.");
  g_print ("Found %u mpris player(s):\n", l);
  for (i = 0; i < l; i++)
    g_print ("\t%s\n", names[i] + plen);
}

static const gchar *
get_description ()
{
  return "Available Commands:"
      "\n  open [URL]              Open the specified URL"
      "\n  play                    Command the player to play"
      "\n  pause                   Command the player to pause"
      "\n  play-pause              Command the player to toggle between play/pause"
      "\n  stop                    Command the player to stop"
      "\n  next                    Command the player to skip to the next track"
      "\n  previous                Command the player to skip to the previous track"
      "\n  cycle-subs              Command the player to cycle through the available subtitles"
      "\n  position [OFFSET][+/-]  Command the player to go to the position or seek forward/backward OFFSET in seconds"
      "\n  volume [LEVEL]          Print or set the volume to LEVEL from 0.0 to 1.0"
      "\n  status                  Get the play status of the player"
      "\n  metadata [KEY]          Print metadata information for the current track. If KEY is passed,"
      "\n                          print only that value. KEY may be one of artist, title or album"
      "\n  loop [TYPE]             Print the loop status. If TYPE is passed it will set"
      "\n                          TYPE can be [none | track | playlist]"
      "\n  rate [RATE]             Print the current playback rate or set to RATE from (1.0..]"
      "\n  shuffle [SHUFFLE]       Print the shuffle state or set to SHUFFLE [Y | N]"
      "\n  quit                    Causes the media player to stop running"
      "\n  raise                   Brings the media player's user interface to the front"
      "\n  fs                      Flips fullscreen mode";
}

static gboolean
parse_options (int *argc, char ***argv)
{
  static const GOptionEntry entries[] = {
    {"player", 'p', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &opt_player_name,
        "The name of the player to control (default: dplayer)", "NAME"},
    {"list-all", 'l', G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, &opt_list,
          "List the names of running players that can be controlled and exit",
        NULL},
    {G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_STRING_ARRAY, &opt_command, NULL,
        "COMMAND…"},
    {NULL}
  };
  static const gchar *summary =
      "  Only for players supporting the MPRIS D-Bus specification";

  GOptionContext *ctxt;
  gboolean ret = FALSE;
  GError *err = NULL;
  gint i;

  ctxt = g_option_context_new ("— Controller for MPRIS players");
  g_option_context_add_main_entries (ctxt, entries, NULL);
  g_option_context_set_description (ctxt, get_description ());
  g_option_context_set_summary (ctxt, summary);

  if (!g_option_context_parse (ctxt, argc, argv, &err)) {
    g_print ("Parsing error: %s\n", err->message);
    g_clear_error (&err);
    goto bail;
  }

  /* no command == list players */
  if (opt_list || (!opt_command && !opt_list)) {
    /* list player, then no command */
    if (opt_command)
      g_strfreev (opt_command);
    ret = TRUE;
    goto bail;
  }

  /* our command */
  for (i = 0; i < G_N_ELEMENTS (CMDS); i++) {
    if ((g_ascii_strncasecmp (opt_command[0], CMDS[i].name,
                strlen (CMDS[i].name)) == 0)) {
      ret = TRUE;
      break;
    }
  }

  for (i = 0; i < G_N_ELEMENTS (CMDS_APP); i++) {
      if ((g_ascii_strncasecmp (opt_command[0], CMDS_APP[i].name,
                strlen (CMDS_APP[i].name)) == 0)) {
      ret = TRUE;
      break;
    }
  }

  if (!ret) {
    gchar *data = g_strjoinv (" ", opt_command);
    g_print ("Invalid command: %s\n", data);
    g_free (data);
  }

bail:
  g_option_context_free (ctxt);
  return ret;
}

int
main (int argc, char **argv)
{
  gboolean ret;
  gchar **player_names;

  setlocale (LC_ALL, "");

  ret = parse_options (&argc, &argv);
  if (!ret)
    return EXIT_FAILURE;

  ret = FALSE;
  player_names = get_player_names ();
  if (opt_list) {
    if (player_names) {
      print_player_names (player_names);
    } else {
      g_print ("No MPRIS players found\n");
    }
    ret = TRUE;
  } else if (opt_command) {
    ret = process_command (get_dbus_name (player_names));
  } else {
    g_print ("No command to process\n");
  }

  g_free (opt_player_name);
  if (opt_command)
    g_strfreev (opt_command);
  if (player_names)
    g_strfreev (player_names);

  return ret ? EXIT_SUCCESS : EXIT_FAILURE;
}
