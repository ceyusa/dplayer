/*
 * Copyright (c) 2017 Víctor Jáquez <vjaquez@igalia.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Control Center; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "dmp-player.h"
#include "dmp-player-skel.h"
#include "dmp-service.h"
#include "dmp-service-private.h"
#include "org.mpris.MediaPlayer2.h"

#include <gst/gst.h>

#define MAX_RATE 5.0
#define MIN_RATE 0.1

struct _DmpService
{
  GtkApplication parent;

  DmpMediaPlayer2 *app_skel;
  DmpMediaPlayer2Player *player_skel;

  guint inhibit_cookie;
  GstPlayerState player_state;

  gchar **supported_uri_schemes;
};

G_DEFINE_TYPE (DmpService, dmp_service, GTK_TYPE_APPLICATION)

static gchar **
get_available_uri_handlers ()
{
  GList *plugins, *pitem;
  GArray *uri_handlers;

  uri_handlers = g_array_new (TRUE, FALSE, sizeof (gchar *));
  plugins = gst_registry_get_plugin_list (gst_registry_get ());
  for (pitem = plugins; pitem; pitem = pitem->next) {
    GstPlugin *plugin;
    GList *features, *fitem;

    plugin = (GstPlugin *) pitem->data;
    if (GST_OBJECT_FLAG_IS_SET (plugin, GST_PLUGIN_FLAG_BLACKLISTED))
      continue;

    features = gst_registry_get_feature_list_by_plugin (gst_registry_get (),
        gst_plugin_get_name (plugin));
    for (fitem = features; fitem; fitem = fitem->next) {
      GstPluginFeature *feature;
      GstElementFactory *factory;
      GstElement *element;
      GstURIType uritype;
      const gchar *const *uri_protocols;
      guint len, i;

      feature = GST_PLUGIN_FEATURE (fitem->data);
      if (!GST_IS_ELEMENT_FACTORY (feature))
        continue;
      factory = GST_ELEMENT_FACTORY (gst_plugin_feature_load (feature));
      if (!factory)
        continue;
      element = gst_element_factory_create (factory, NULL);
      gst_object_unref (factory);
      if (!GST_IS_URI_HANDLER (element)) {
        gst_object_unref (element);
        continue;
      }
      uritype = gst_uri_handler_get_uri_type (GST_URI_HANDLER (element));
      if (uritype != GST_URI_SRC) {
        gst_object_unref (element);
        continue;
      }

      uri_protocols =
        gst_uri_handler_get_protocols (GST_URI_HANDLER (element));
      gst_object_unref (element);
      if (!uri_protocols)
        continue;

      len = g_strv_length ((gchar **) uri_protocols);
      for (i = 0; i < len; i++){
        gchar *uri_scheme;
        gboolean found = FALSE;
        guint j;

        for (j = 0; j < uri_handlers->len; j++) {
          gchar *uri_prot;

          uri_prot = g_array_index (uri_handlers, gchar *, j);
          if (g_strcmp0 (uri_prot, uri_protocols[i]) == 0) {
            found = TRUE;
            break;
          }
        }

        if (found)
          continue;

        uri_scheme = g_strdup (uri_protocols[i]);
        g_array_append_val (uri_handlers, uri_scheme);
      }
    }

    gst_plugin_feature_list_free (features);
  }

  gst_plugin_list_free (plugins);
  return (gchar **) g_array_free (uri_handlers, FALSE);
}

static void
app_session_set_idle (DmpService * self, gboolean idle)
{
  GtkApplication *app = GTK_APPLICATION (self);
  GtkWindow *active;

  if (self->inhibit_cookie)
    gtk_application_uninhibit (app, self->inhibit_cookie);

  if (idle) {
    self->inhibit_cookie = 0;
    return;
  }

  active = gtk_application_get_active_window (app);
  if (!active)
    return;

  self->inhibit_cookie = gtk_application_inhibit (app, active,
        GTK_APPLICATION_INHIBIT_IDLE, "Playing media");
}

static DmpPlayer *
get_player (DmpService * self)
{
  GtkApplication *app = GTK_APPLICATION (self);
  GtkWindow *active;
  DmpPlayer *player;

  active = gtk_application_get_active_window (app);
  if (active)
    return DMP_PLAYER (active);

  player = dmp_player_new ();
  gtk_application_add_window (app, GTK_WINDOW (player));

  return player;
}

#define GET_PLAYER(x) dmp_player_get_player (get_player ((x)))

static void
service_quit (DmpService * self)
{
  GtkApplication *app = GTK_APPLICATION (self);
  GtkWindow *active;

  active = gtk_application_get_active_window (app);
  if (active) {
    gtk_application_remove_window (app, active);
    gtk_widget_destroy (GTK_WIDGET (active));
  }

  app_session_set_idle (self, TRUE);
  g_application_quit (G_APPLICATION (self));
}

static gboolean
handle_quit (DmpMediaPlayer2 * skeleton, GDBusMethodInvocation * invocation,
    DmpService * self)
{
  service_quit (self);
  dmp_media_player2_complete_quit (skeleton, invocation);
  return TRUE;
}

static gboolean
handle_open_uri (DmpMediaPlayer2Player * skeleton,
    GDBusMethodInvocation * invocation, gchar * uri, DmpService * self)
{
  dmp_player_open (get_player (self), uri);
  app_session_set_idle (self, FALSE);
  dmp_player_play (get_player (self));
  dmp_media_player2_player_complete_open_uri (skeleton, invocation);
  return TRUE;
}

static gboolean
handle_play (DmpMediaPlayer2Player * skeleton,
    GDBusMethodInvocation * invocation, DmpService * self)
{
  if (self->player_state != GST_PLAYER_STATE_PLAYING) {
    dmp_player_play (get_player (self));
    app_session_set_idle (self, FALSE);
  }
  dmp_media_player2_player_complete_play (skeleton, invocation);
  return TRUE;
}

static gboolean
handle_pause (DmpMediaPlayer2Player * skeleton,
    GDBusMethodInvocation * invocation, DmpService * self)
{
  dmp_player_pause (get_player (self));
  app_session_set_idle (self, TRUE);
  dmp_media_player2_player_complete_pause (skeleton, invocation);
  return TRUE;
}

static gboolean
handle_playpause (DmpMediaPlayer2Player * skeleton,
    GDBusMethodInvocation * invocation, DmpService * self)
{
  if (self->player_state == GST_PLAYER_STATE_PAUSED) {
    dmp_player_play (get_player (self));
    app_session_set_idle (self, FALSE);
  } else if (self->player_state == GST_PLAYER_STATE_PLAYING) {
    dmp_player_pause (get_player (self));
    app_session_set_idle (self, TRUE);
  }
  dmp_media_player2_player_complete_play_pause (skeleton, invocation);
  return TRUE;
}

static void
clear_metadata (DmpService * self)
{
  dmp_media_player2_player_set_metadata (self->player_skel, NULL);
  dmp_media_player2_player_set_can_seek (self->player_skel, FALSE);
}

static gboolean
handle_stop (DmpMediaPlayer2Player * skeleton,
    GDBusMethodInvocation * invocation, DmpService * self)
{
  dmp_player_stop (get_player (self));
  app_session_set_idle (self, TRUE);
  clear_metadata (self);

  dmp_media_player2_player_complete_stop (skeleton, invocation);
  return TRUE;
}

static gboolean
handle_seek (DmpMediaPlayer2Player * skeleton,
    GDBusMethodInvocation * invocation, gint64 offset, DmpService * self)
{
  dmp_player_seek (get_player (self), offset);
  dmp_media_player2_player_complete_seek (skeleton, invocation);
  return TRUE;
}

static gboolean
handle_set_position (DmpMediaPlayer2Player * skeleton,
    GDBusMethodInvocation * invocation, const char * trackid, gint64 position,
    DmpService * self)
{
  GstClockTime new_position;

  if (position < 0)
    goto bail;

  if (g_strcmp0 (trackid, "/org/mpris/MediaPlayer2/TrackList/0") != 0)
    goto bail;

  new_position = gst_util_uint64_scale (position, GST_USECOND, 1);
  dmp_player_set_position (get_player (self), new_position);

bail:
  dmp_media_player2_player_complete_set_position (skeleton, invocation);
  return TRUE;
}

static gboolean
handle_cycle_subtitles (DmpMediaPlayer2Player * skeleton,
    GDBusMethodInvocation * invocation, DmpService * self)
{
  gchar *sub_desc;

  sub_desc = dmp_player_set_next_subtitle (get_player (self));

  dmp_media_player2_player_complete_cycle_subtitles (skeleton, invocation,
      sub_desc);
  return TRUE;
}

void
dmp_service_player_set_media_info (DmpService * self, DmpMediaInfo * info)
{
  GVariantBuilder builder;
  GVariant *value;
  guint64 duration;
  gchar *trackid, *basename;
  const gchar *title, *uri;
  gboolean seekable;

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{sv}"));

  duration =
      GST_TIME_AS_USECONDS (gst_player_media_info_get_duration (info->info));
  g_variant_builder_add (&builder, "{sv}", "mpris:length",
      g_variant_new_uint64 (duration));

  trackid = g_strdup_printf ("/org/mpris/MediaPlayer2/TrackList/%d", 0);
  g_variant_builder_add (&builder, "{sv}", "mpris:trackid",
      g_variant_new_object_path (trackid));
  g_free (trackid);

  uri = info->uri;
  if (!uri)
    uri = gst_player_media_info_get_uri (info->info);
  g_variant_builder_add (&builder, "{sv}", "xesam:url",
      g_variant_new_string (uri));

  basename = NULL;
  title = info->title;
  if (!title) {
    title = gst_player_media_info_get_title (info->info);
    if (!title) {
      uri = gst_player_media_info_get_uri (info->info);
      basename = g_path_get_basename (uri);
    }
  }
  g_variant_builder_add (&builder, "{sv}", "xesam:title",
      g_variant_new_string (title ? title : basename));
  g_free (basename);

  if (info->subtitle_id) {
    g_variant_builder_add (&builder, "{sv}", "subtitle",
        g_variant_new_string (info->subtitle_id));
  }

  value = g_variant_new ("a{sv}", &builder);
  dmp_media_player2_player_set_metadata (self->player_skel, value);

  seekable = gst_player_media_info_is_seekable (info->info);
  dmp_media_player2_player_set_can_seek (self->player_skel, seekable);
}

void
dmp_service_player_set_state (DmpService * self, GstPlayerState state)
{
  const gchar *name;

  switch (state) {
    case GST_PLAYER_STATE_STOPPED:
      name = "Stopped";
      clear_metadata (self);
      break;
    case GST_PLAYER_STATE_BUFFERING:
    case GST_PLAYER_STATE_PAUSED:
      name = "Paused";
      break;
    case GST_PLAYER_STATE_PLAYING:
      name = "Playing";
      break;
    default:
      name = NULL;
      break;
  }

  self->player_state = state;
  dmp_media_player2_player_set_playback_status (self->player_skel, name);
}

guint64
dmp_service_player_get_position (DmpService * self)
{
  g_return_val_if_fail (self, 0);
  if (self->player_state == GST_PLAYER_STATE_STOPPED)
    return 0;
  return gst_player_get_position (GET_PLAYER (self));
}

gdouble
dmp_service_player_get_volume (DmpService * self)
{
  g_return_val_if_fail (self, -1);
  return dmp_player_get_volume (get_player (self));
}

void
dmp_service_player_set_volume (DmpService * self, gdouble volume)
{
  g_return_if_fail (self);
  dmp_player_set_volume (get_player (self), volume);
}

gdouble
dmp_service_player_get_rate (DmpService * self)
{
  g_return_val_if_fail (self, 0);
  return gst_player_get_rate (GET_PLAYER (self));
}

void
dmp_service_player_set_rate (DmpService * self, gdouble rate)
{
  g_return_if_fail (self);

  if (rate == 0) {
    dmp_player_pause (get_player (self));
    app_session_set_idle (self, TRUE);
  } else if (rate >= MIN_RATE && rate <= MAX_RATE) {
    gst_player_set_rate (GET_PLAYER (self), rate);
  }
}

void
dmp_service_player_emit_seeked (DmpService * self, gint64 position)
{
  dmp_media_player2_player_emit_seeked (self->player_skel, position);
}

void
dmp_service_player_quit (DmpService * self)
{
  service_quit (self);
}

static void
dmp_service_init (DmpService * self)
{
}

static void
dmp_service_startup (GApplication * application)
{
  G_APPLICATION_CLASS (dmp_service_parent_class)->startup (application);

  g_application_hold (application);
}

static gboolean
dmp_service_dbus_register (GApplication * application,
    GDBusConnection * connection, const gchar * object_path, GError ** error)
{
  DmpService *self = DMP_SERVICE (application);
  GDBusInterfaceSkeleton *skeleton;

  /* @TODO: query gstreamer's registry to expose this */
  const gchar *const supported_mime_types[] = { "video/x-ms-asf",
    "video/x-msvideo", "video/mpeg4", "video/quicktime", "video/x-matroska",
    NULL
  };

  self->app_skel = dmp_media_player2_skeleton_new ();
  dmp_media_player2_set_can_quit (self->app_skel, TRUE);
  dmp_media_player2_set_fullscreen (self->app_skel, TRUE);
  dmp_media_player2_set_identity (self->app_skel, "DBus media player");
  dmp_media_player2_set_desktop_entry (self->app_skel, "dplayer");

  if (!self->supported_uri_schemes)
    self->supported_uri_schemes = get_available_uri_handlers ();
  dmp_media_player2_set_supported_uri_schemes (self->app_skel,
      (const gchar *const *) self->supported_uri_schemes);

  dmp_media_player2_set_supported_mime_types (self->app_skel,
      supported_mime_types);
  g_signal_connect (self->app_skel, "handle-quit", G_CALLBACK (handle_quit),
      self);

  skeleton = G_DBUS_INTERFACE_SKELETON (self->app_skel);
  if (!g_dbus_interface_skeleton_export (skeleton, connection,
          "/org/mpris/MediaPlayer2", error))
    return FALSE;

  self->player_skel = dmp_player_skel_new ();
  dmp_media_player2_player_set_can_control (self->player_skel, TRUE);
  dmp_media_player2_player_set_can_play (self->player_skel, TRUE);
  g_signal_connect (self->player_skel, "handle-open-uri",
      G_CALLBACK (handle_open_uri), self);
  g_signal_connect (self->player_skel, "handle-play", G_CALLBACK (handle_play),
      self);
  g_signal_connect (self->player_skel, "handle-play-pause",
      G_CALLBACK (handle_playpause), self);
  dmp_media_player2_player_set_can_pause (self->player_skel, TRUE);
  g_signal_connect (self->player_skel, "handle-pause",
      G_CALLBACK (handle_pause), self);
  g_signal_connect (self->player_skel, "handle-stop", G_CALLBACK (handle_stop),
      self);
  g_signal_connect (self->player_skel, "handle-seek", G_CALLBACK (handle_seek),
      self);
  g_signal_connect (self->player_skel, "handle-set-position",
      G_CALLBACK (handle_set_position), self);
  g_signal_connect (self->player_skel, "handle-cycle-subtitles",
      G_CALLBACK (handle_cycle_subtitles), self);

  dmp_media_player2_player_set_playback_status (self->player_skel, "Stopped");
  dmp_media_player2_player_set_loop_status (self->player_skel, "None");

  dmp_media_player2_player_set_maximum_rate (self->player_skel, MAX_RATE);
  dmp_media_player2_player_set_minimum_rate (self->player_skel, MIN_RATE);

  skeleton = G_DBUS_INTERFACE_SKELETON (self->player_skel);
  return g_dbus_interface_skeleton_export (skeleton, connection,
      "/org/mpris/MediaPlayer2", error);
}

static void
dmp_service_dbus_unregister (GApplication * application,
    GDBusConnection * connection, const gchar * object_path)
{
  DmpService *self = DMP_SERVICE (application);
  GDBusInterfaceSkeleton *skeleton;

  skeleton = G_DBUS_INTERFACE_SKELETON (self->app_skel);
  if (g_dbus_interface_skeleton_has_connection (skeleton, connection))
    g_dbus_interface_skeleton_unexport_from_connection (skeleton, connection);
  g_clear_object (&self->app_skel);

  skeleton = G_DBUS_INTERFACE_SKELETON (self->player_skel);
  if (g_dbus_interface_skeleton_has_connection (skeleton, connection))
    g_dbus_interface_skeleton_unexport_from_connection (skeleton, connection);
  g_clear_object (&self->player_skel);
}

static void
dmp_service_dispose (GObject * object)
{
  DmpService *self = DMP_SERVICE (object);

  g_clear_pointer (&self->supported_uri_schemes, g_strfreev);
  G_OBJECT_CLASS (dmp_service_parent_class)->dispose (object);
}

static void
dmp_service_class_init (DmpServiceClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *application_class = G_APPLICATION_CLASS (klass);

  object_class->dispose = dmp_service_dispose;

  application_class->startup = dmp_service_startup;
  application_class->dbus_register = dmp_service_dbus_register;
  application_class->dbus_unregister = dmp_service_dbus_unregister;
}

DmpService *
dmp_service_new (void)
{
  DmpService *self;

  g_set_prgname ("dplayer");
  g_set_application_name ("dplayer");

  g_setenv ("PULSE_PROP_media.role", "video", TRUE);
  g_setenv ("GST_GL_WINDOW", "x11", TRUE);
  g_setenv ("GST_GL_API", "opengl", TRUE);
  g_setenv ("GST_GL_PLATFORM", "glx", TRUE);

  self = g_object_new (DMP_TYPE_SERVICE, "flags", G_APPLICATION_IS_SERVICE,
      "application_id", "org.mpris.MediaPlayer2.dplayer", NULL);

  g_application_set_default (G_APPLICATION (self));
  g_application_add_option_group (G_APPLICATION (self),
      gst_init_get_option_group ());

  return self;
}
