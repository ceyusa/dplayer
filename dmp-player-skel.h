/*
 * Copyright (c) 2017 Víctor Jáquez <vjaquez@igalia.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Control Center; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#pragma once

#include "org.mpris.MediaPlayer2.Player.h"

G_BEGIN_DECLS

#define DMP_TYPE_PLAYER_SKEL (dmp_player_skel_get_type ())
#define DMP_PLAYER_SKEL(o) \
  (G_TYPE_CHECK_INSTANCE_CAST ((o), DMP_TYPE_PLAYER_SKEL, DmpPlayerSkel))
#define DMP_PLAYER_SKEL_CLASS(k) \
  (G_TYPE_CHECK_CLASS_CAST ((k), DMP_TYPE_PLAYER_SKEL, DmpPlayerSkelClass))
#define DMP_PLAYER_SKEL_GET_CLASS(o) \
  (G_TYPE_INSTANCE_GET_CLASS ((o), DMP_TYPE_PLAYER_SKEL, DmpPlayerSkelClass))
#define DMP_IS_PLAYER_SKEL(o) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((o), DMP_TYPE_PLAYER_SKEL))
#define DMP_IS_PLAYER_SKEL_CLASS(k) \
  (G_TYPE_CHECK_CLASS_TYPE ((k), DMP_TYPE_PLAYER_SKEL))

typedef struct _DmpPlayerSkel DmpPlayerSkel;
typedef struct _DmpPlayerSkelClass DmpPlayerSkelClass;

struct _DmpPlayerSkel
{
  DmpMediaPlayer2PlayerSkeleton parent;
};

struct _DmpPlayerSkelClass
{
  DmpMediaPlayer2PlayerSkeletonClass parent_class;
};

GType dmp_player_skel_get_type (void) G_GNUC_CONST;

DmpMediaPlayer2Player *dmp_player_skel_new (void);

G_END_DECLS
