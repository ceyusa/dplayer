#!/bin/sh -e

mkdir --parents spec
cp mpris-spec/spec/org.mpris.MediaPlayer2.xml spec/
cp mpris-spec/spec/org.mpris.MediaPlayer2.Player.xml spec/
patch --batch --quiet --strip=1 < mpris-patches/0001-Add-Cycle-Subtitle-method.patch
