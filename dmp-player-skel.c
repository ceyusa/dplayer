/*
 * Copyright (c) 2017 Víctor Jáquez <vjaquez@igalia.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Control Center; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "dmp-player-skel.h"
#include "dmp-service-private.h"

enum
{
  PROP_RATE = 1,
  PROP_VOLUME,
  PROP_POSITION,
};

G_DEFINE_TYPE (DmpPlayerSkel, dmp_player_skel, DMP_TYPE_MEDIA_PLAYER2_PLAYER_SKELETON)

static void
dmp_player_skel_set_property (GObject *object, guint propid,
    const GValue *value, GParamSpec *pspec)
{
  DmpService *service = DMP_SERVICE (g_application_get_default ());
  g_assert (service);

  switch (propid) {
    case PROP_RATE: {
      gdouble rate ;
      rate = g_value_get_double (value);
      dmp_service_player_set_rate (service, rate);
      break;
    }
    case PROP_VOLUME: {
      gdouble volume = g_value_get_double (value);
      dmp_service_player_set_volume (service, volume);
      break;
    }
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, propid, pspec);
      break;
  }
}

static void
dmp_player_skel_get_property (GObject *object, guint propid, GValue *value,
    GParamSpec *pspec)
{
  DmpService *service = DMP_SERVICE (g_application_get_default ());
  g_assert (service);

  switch (propid) {
    case PROP_RATE: {
      gdouble rate;
      rate = dmp_service_player_get_rate (service);
      g_value_set_double (value, rate);
      break;
    }
    case PROP_VOLUME: {
      gdouble volume;
      volume = dmp_service_player_get_volume (service);
      g_value_set_double (value, volume);
      break;
    }
    case PROP_POSITION: {
      guint64 position;
      position = dmp_service_player_get_position (service);
      g_value_set_int64 (value, GST_TIME_AS_USECONDS (position));
      break;
    }
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, propid, pspec);
      break;
  }
}

static void
dmp_player_skel_init (DmpPlayerSkel * self)
{
}

static void
dmp_player_skel_class_init (DmpPlayerSkelClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = dmp_player_skel_set_property;
  object_class->get_property = dmp_player_skel_get_property;

  g_object_class_override_property (object_class, PROP_RATE, "rate");
  g_object_class_override_property (object_class, PROP_VOLUME, "volume");
  g_object_class_override_property (object_class, PROP_POSITION, "position");
}

DmpMediaPlayer2Player *
dmp_player_skel_new (void)
{
  return DMP_MEDIA_PLAYER2_PLAYER (g_object_new (DMP_TYPE_PLAYER_SKEL, NULL));
}
