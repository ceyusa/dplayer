/*
 * Copyright (c) 2017 Víctor Jáquez <vjaquez@igalia.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Control Center; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "dmp-player.h"
#include "dmp-service-private.h"
#include "gtk-video-renderer.h"
#include "dmp-config.h"

#include <gdk/gdk.h>
#if defined (GDK_WINDOWING_X11)
#include <gdk/gdkx.h>
#elif defined (GDK_WINDOWING_WIN32)
#include <gdk/gdkwin32.h>
#elif defined (GDK_WINDOWING_QUARTZ)
#include <gdk/gdkquartz.h>
#endif
#include <glib/gstdio.h>
#include <stdlib.h>
#include <string.h>

struct _DmpPlayer
{
  GtkApplicationWindow parent;

  GstPlayer *player;
  GstPlayerVideoRenderer *renderer;

  GtkBuilder *builder;

  GtkWidget *video_area;

  GtkWidget *toolbox;
  GtkWidget *progressbar;
  GtkWidget *elapsed;
  GtkWidget *remain;

  GtkWidget *osd;

  guint toolbox_timer;
  guint osd_timer;

  GstClockTime requested_position;

  gchar *original_uri;
  gchar *stream_title;

  GList *external_subtitles;
  guint subtitle_idx;
};

G_DEFINE_TYPE (DmpPlayer, dmp_player, GTK_TYPE_APPLICATION_WINDOW);

static gchar *
run_ytdl_cmd (const gchar * uri, gint argc, gchar ** argv)
{
  char **args;
  char *out = NULL;
  int i, c = 0, status = 0;
  GError *err = NULL;
  gboolean ret;

  if (!g_str_has_prefix (uri, "http"))
    return NULL;

  if (!dmp_config_get_ytdl_path ())
    return NULL;

  if (!g_file_test (dmp_config_get_ytdl_path (), G_FILE_TEST_IS_EXECUTABLE))
    return NULL;

  args = g_malloc (sizeof (char *) * (argc + 3));
  args[c++] = (char *) dmp_config_get_ytdl_path ();
  for (i = 0; i < argc; i++)
    args[c++] = (char *) argv[i];
  args[c++] = (char *) uri;
  args[c++] = NULL;

  ret = g_spawn_sync (NULL, args, NULL, G_SPAWN_STDERR_TO_DEV_NULL, NULL, NULL,
      &out, NULL, &status, &err);

  g_free (args);

  if (err) {
    g_warning ("Couldn't read ytdl: %s", err->message);
    g_error_free (err);
  }

  if (!ret || status != 0 || !out) {
    g_free (out);
    return NULL;
  }
  return out;
}

static gchar *
get_stream_info (gchar * uri)
{
  gchar *argv[] = { "--get-title", "--format", "best", "--get-url" };

  return run_ytdl_cmd (uri, 4, argv);
}

static gchar *
get_resume_filename (const gchar * uri)
{
  gchar *path, *csum, *fname;

  csum = fname = NULL;
  path = g_build_filename (g_get_user_config_dir (), "dplayer", "watch_later", NULL);
  if (g_mkdir_with_parents (path, 0750) == -1) {
    g_warning ("Error creating directory %s: %s", path, g_strerror (errno));
    goto bail;
  }

  csum = g_compute_checksum_for_string (G_CHECKSUM_MD5, uri, -1);
  fname = g_build_filename (path, csum, NULL);

bail:
  g_free (path);
  g_free (csum);
  return fname;
}

static gboolean
can_watch_later (const GstPlayerMediaInfo * minfo)
{
  if (!minfo)
    return FALSE;
  if (str2bool (dmp_config_get_watch_later ()) != TRUE)
    return FALSE;
  if (!gst_player_media_info_is_seekable (minfo))
    return FALSE;

  return TRUE;
}

static void
write_watch_later (DmpPlayer * self)
{
  GstPlayerMediaInfo *minfo;
  const gchar *uri;
  gchar *resumefn, *posstr;
  GstClockTime pos;
  GError *err = NULL;

  resumefn = posstr = NULL;
  minfo = gst_player_get_media_info (self->player);
  if (!can_watch_later (minfo))
    goto bail;

  uri = gst_player_media_info_get_uri (minfo);
  if (!uri)
    goto bail;
  if (g_str_has_prefix (uri, "http")
      && g_strcmp0 (self->original_uri, uri) != 0)
    uri = self->original_uri;

  resumefn = get_resume_filename (uri);
  if (!resumefn)
    goto bail;

  pos = gst_player_get_position (self->player);
  if (pos == GST_CLOCK_TIME_NONE)
    goto bail;
  posstr = g_strdup_printf ("%" G_GUINT64_FORMAT, pos);

  if (!g_file_set_contents (resumefn, posstr, -1, &err)) {
    g_warning ("Error writing file %s: %s", resumefn, err->message);
    g_error_free (err);
  }

bail:
  if (minfo)
    g_object_unref (minfo);
  g_free (resumefn);
  g_free (posstr);
  return;
}

static void
load_watch_later (DmpPlayer * self, const gchar * uri)
{
  gchar *resumefn, *content, *eof;
  gsize length = 0;
  GError *err = NULL;
  GstClockTime reqpos;

  resumefn = content = NULL;
  resumefn = get_resume_filename (uri);
  if (!resumefn)
    goto bail;

  if (!g_file_test (resumefn, G_FILE_TEST_EXISTS || G_FILE_TEST_IS_REGULAR))
    goto bail;

  if (!g_file_get_contents (resumefn, &content, &length, &err)) {
    g_warning ("Error reading file %s: %s", resumefn, err->message);
    g_error_free (err);
    goto bail;
  }
  if (!content || *content == '\0')
    goto bail;

  reqpos = g_ascii_strtoull (content, &eof, 10);
  if (errno == ERANGE || errno == EINVAL)
    goto bail;
  if (*eof != '\0' && !g_ascii_isspace (*eof))
    goto bail;
  if (reqpos == GST_CLOCK_TIME_NONE || reqpos == 0)
    goto bail;

  self->requested_position = reqpos;

bail:
  if (resumefn) {
    g_unlink (resumefn);
    g_free (resumefn);
  }

  g_free (content);

  return;
}

static void
toolbox_set_title (DmpPlayer * self, const gchar * title)
{
  GtkWidget *label;

  label = GTK_WIDGET (gtk_builder_get_object (self->builder, "title_label"));
  gtk_label_set_text (GTK_LABEL (label), title);
}

static void
player_stop_decorator (DmpPlayer * self, gboolean watch_later)
{
  if (watch_later)
    write_watch_later (self);

  gst_player_stop (self->player);
  gtk_widget_hide (GTK_WIDGET (self));
}

static void
reset_internal_state (DmpPlayer * self)
{
  toolbox_set_title (self, NULL);
  g_clear_pointer (&self->original_uri, g_free);
  g_clear_pointer (&self->stream_title, g_free);

  g_list_free_full (self->external_subtitles, g_free);
  self->external_subtitles = NULL;

  self->subtitle_idx = 0;
}

static void
apply_watch_later (DmpPlayer * self)
{
  GstPlayerMediaInfo *minfo;
  GstClockTime duration, curpos;

  if (self->requested_position == GST_CLOCK_TIME_NONE)
    return;
  minfo = gst_player_get_media_info (self->player);
  if (!minfo)
    return;
  duration = gst_player_media_info_get_duration (minfo);
  if (self->requested_position > duration)
    goto bail;
  curpos = gst_player_get_position (self->player);
  if (self->requested_position < curpos)
    goto bail;

  gst_player_seek (self->player, self->requested_position);

bail:
  self->requested_position = GST_CLOCK_TIME_NONE;
  if (minfo)
    g_object_unref (minfo);
  return;
}

static GList *
find_external_subtitles (GList * subs, const gchar *uri)
{
  guint i;
  gchar *prot, *loc, *name, *dir, *ext;
  const gchar *fname, *extensions[] = { ".srt", ".sub", ".ass", NULL };
  GError *err = NULL;
  GDir *cdir;
  gboolean is_file;

  prot = gst_uri_get_protocol (uri);
  is_file = (g_strcmp0 (prot, "file") == 0);
  g_free (prot);
  if (!is_file)
    return NULL;

  loc = g_filename_from_uri (uri, NULL, NULL);
  if (!loc)
    return NULL;
  name = g_path_get_basename (loc);
  dir = g_path_get_dirname (loc);
  g_free (loc);

  /* let's remove the extension */
  ext = g_utf8_strrchr (name, -1, '.');
  if (ext) {
    gchar *old = name;
    name = g_strndup (name, ext - name);
    g_free (old);
  }

  cdir = g_dir_open (dir, 0, &err);
  if (cdir == NULL) {
    g_warning ("cannot open %s: %s", dir, err->message);
    g_error_free (err);
    goto error_no_name;
  }

  /* look for all suitable subtitles in the local directory:
  *  like regex filename.*\.ext */
  while ((fname = g_dir_read_name (cdir)) != NULL) {
    if (!g_str_has_prefix (fname, name))
      continue;

    for (i = 0; extensions[i]; i++) {
      if (!g_str_has_suffix (fname, extensions[i]))
        continue;

      loc = g_build_filename (dir, fname, NULL);
      if (g_file_test (loc, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR)) {
        gchar *suburi = g_filename_to_uri (loc, NULL, NULL);
        if (suburi)
          subs = g_list_append (subs, suburi);
      }
      g_free (loc);
    }
  }
  g_dir_close (cdir);

error_no_name:
  g_free (name);
  g_free (dir);

  return subs;
}

static void
video_area_realize_cb (GtkWidget * widget, DmpPlayer * self)
{
  GdkWindow *window = gtk_widget_get_window (widget);
  guintptr handle;

  if (!gdk_window_ensure_native (window))
    g_error ("Couldn't create native window needed for GstXOverlay!");

#if defined (GDK_WINDOWING_WIN32)
  handle = (guintptr) GDK_WINDOW_HWND (window);
#elif defined (GDK_WINDOWING_QUARTZ)
  handle = gdk_quartz_window_get_nsview (window);
#elif defined (GDK_WINDOWING_X11)
  handle = GDK_WINDOW_XID (window);
#endif

  g_object_set (self->renderer, "window-handle", (gpointer) handle, NULL);
}

static inline void
hide_mouse_pointer (DmpPlayer * self)
{
  GdkCursor *cursor;
  GdkDisplay *display;
  GdkWindow *window;

  display = gtk_widget_get_display (GTK_WIDGET (self));
  cursor = gdk_cursor_new_for_display (display, GDK_BLANK_CURSOR);

  window = gtk_widget_get_window (self->video_area);
  gdk_window_set_cursor (window, cursor);

  g_object_unref (cursor);
}

static void
toolbox_show (DmpPlayer * self)
{
  /* if timer is running then kill it */
  if (self->toolbox_timer) {
    g_source_remove (self->toolbox_timer);
    self->toolbox_timer = 0;
  }

  gtk_widget_show (self->toolbox);
}

static gboolean
toolbox_hide_cb (gpointer data)
{
  DmpPlayer *self = DMP_PLAYER (data);

  gtk_widget_hide (self->toolbox);
  self->toolbox_timer = 0;
  return FALSE;
}

static void
toolbox_start_timer (DmpPlayer * self)
{
  /* start timer to hide toolbox */
  if (self->toolbox_timer)
    g_source_remove (self->toolbox_timer);
  self->toolbox_timer =
    g_timeout_add_seconds (5, toolbox_hide_cb, self);
}

static gboolean
overlay_get_child_position_cb (GtkOverlay * overlay, GtkWidget * widget,
    GtkAllocation * alloc, DmpPlayer * self)
{
  GtkRequisition req;
  GtkWidget *child;
  GtkAllocation main_alloc;
  gint x, y;
  GtkWidget *relative;

  relative = self->video_area;

  child = gtk_bin_get_child (GTK_BIN (overlay));
  gtk_widget_translate_coordinates (relative, child, 0, 0, &x, &y);
  main_alloc.x = x;
  main_alloc.y = y;
  main_alloc.width = gtk_widget_get_allocated_width (relative);
  main_alloc.height = gtk_widget_get_allocated_height (relative);

  gtk_widget_get_preferred_size (widget, NULL, &req);

  if (widget == self->toolbox)
    alloc->x = (main_alloc.width - req.width) / 2;
  else
    alloc->x = 10;
  if (alloc->x < 0)
    alloc->x = 0;
  alloc->width = MIN (main_alloc.width, req.width);
  if (gtk_widget_get_halign (widget) == GTK_ALIGN_END)
    alloc->x += main_alloc.width - req.width;

  if (widget == self->toolbox)
    alloc->y = main_alloc.height - req.height - 20;
  else
    alloc->y = 10;
  if (alloc->y < 0)
    alloc->y = 0;
  alloc->height = MIN (main_alloc.height, req.height);
  if (gtk_widget_get_valign (widget) == GTK_ALIGN_END)
    alloc->y += main_alloc.height - req.height;

  return TRUE;
}

static void
style_context_add_provider_cb (GtkWidget * widget, GtkStyleProvider * provider)
{
  GtkStyleContext *ctxt;

  ctxt = gtk_widget_get_style_context (widget);
  gtk_style_context_add_provider (ctxt, provider,
      GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  if (GTK_IS_CONTAINER (widget))
    gtk_container_forall (GTK_CONTAINER (widget),
        (GtkCallback) style_context_add_provider_cb, provider);
}

static inline void
toolbox_apply_css (GtkWidget * widget)
{
  GtkCssProvider *provider;
  GBytes *data;

  data = g_resources_lookup_data ("/css/toolbox.css",
      G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);
  if (!data) {
    g_warning ("failed to lookup CSS in resources");
    return;
  }

  provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_data (provider,
      (gchar *) g_bytes_get_data (data, NULL), -1, NULL);
  g_bytes_unref (data);

  style_context_add_provider_cb (widget, GTK_STYLE_PROVIDER (provider));
  g_object_unref (provider);
}

static inline void
create_toolbox_ui (DmpPlayer * self, GtkWidget * overlay)
{
  self->builder = gtk_builder_new_from_resource ("/ui/toolbox.ui");

  self->toolbox =
      GTK_WIDGET (gtk_builder_get_object (self->builder, "toolbox"));
  self->progressbar =
      GTK_WIDGET (gtk_builder_get_object (self->builder, "progressbar"));
  self->elapsed =
      GTK_WIDGET (gtk_builder_get_object (self->builder, "elapsed_time"));
  self->remain =
      GTK_WIDGET (gtk_builder_get_object (self->builder, "remain_time"));

  /* toolbox = GTK_WIDGET (gtk_builder_get_object (self->builder, "toolbox")); */
  toolbox_apply_css (self->toolbox);

  g_signal_connect (overlay, "get-child-position",
      G_CALLBACK (overlay_get_child_position_cb), self);

  gtk_overlay_add_overlay (GTK_OVERLAY (overlay), self->toolbox);
}

static gboolean
osd_hide_cb (gpointer data)
{
  DmpPlayer *self = DMP_PLAYER (data);

  gtk_widget_hide (self->osd);
  gtk_label_set_text (GTK_LABEL (self->osd), NULL);
  self->osd_timer = 0;
  return FALSE;
}

static void
osd_set_text (DmpPlayer * self, const gchar * text)
{
  gst_println ("osd: %s", text);
  gtk_label_set_text (GTK_LABEL (self->osd), text);

  /* if timer is running then kill it */
  if (self->osd_timer) {
    g_source_remove (self->osd_timer);
    self->osd_timer = 0;
  }

  gtk_widget_show (self->osd);

  /* start timer to hide toolbox */
  self->osd_timer =
    g_timeout_add_seconds (5, osd_hide_cb, self);

}

static inline void
create_osd_ui (DmpPlayer * self, GtkWidget * overlay)
{
  self->osd = GTK_WIDGET(gtk_label_new (NULL));
  gtk_widget_set_name (self->osd, "osd_label");
  toolbox_apply_css (self->osd);

  gtk_overlay_add_overlay (GTK_OVERLAY (overlay), self->osd);
}

static void
create_video_overlay_video_renderer (DmpPlayer * self, const gchar *factoryname)
{
  GstElement *sink = NULL;

  if (factoryname)
    sink = gst_element_factory_make (factoryname, NULL);

  if (sink) {
    self->renderer =
      gst_player_video_overlay_video_renderer_new_with_sink (NULL, sink);
  } else {
    self->renderer = gst_player_video_overlay_video_renderer_new (NULL);
  }

  self->video_area = gtk_drawing_area_new ();
  g_signal_connect (self->video_area, "realize",
      G_CALLBACK (video_area_realize_cb), self);
}

static void
create_gtk_video_renderer (DmpPlayer * self)
{
  self->renderer = gst_player_gtk_video_renderer_new ();
  if (self->renderer) {
    self->video_area =
        gst_player_gtk_video_renderer_get_widget (GST_PLAYER_GTK_VIDEO_RENDERER
        (self->renderer));
    g_object_unref (self->video_area);
  } else {
    create_video_overlay_video_renderer (self, NULL);
  }
}

static inline void
create_ui (DmpPlayer * self)
{
  GtkWidget *overlay;
  const gchar *vo;

  overlay = gtk_overlay_new ();
  gtk_container_add (GTK_CONTAINER (self), overlay);
  create_toolbox_ui (self, overlay);
  create_osd_ui (self, overlay);

  gtk_window_fullscreen (GTK_WINDOW (self));
  /* gtk_window_set_keep_above (GTK_WINDOW (self), TRUE); */

  vo = dmp_config_get_video_output ();
  if (vo) {
    create_video_overlay_video_renderer (self, vo);
  } else {
    create_gtk_video_renderer (self);
  }

  gtk_container_add (GTK_CONTAINER (overlay), self->video_area);
  gtk_widget_realize (self->video_area);
  gtk_widget_hide (self->video_area);

  hide_mouse_pointer (self);
}

static inline void
configure_pipeline (DmpPlayer * self)
{
  GstElement *playbin;
  const gchar *fontdesc;

  playbin = gst_player_get_pipeline (self->player);
  fontdesc = dmp_config_get_subtitle_font_desc ();
  if (fontdesc)
    g_object_set (playbin, "subtitle-font-desc", fontdesc, NULL);
  gst_object_unref (playbin);
}

static void
delete_event_cb (GtkWidget * widget, GdkEvent * event, DmpPlayer * self)
{
  player_stop_decorator (self, TRUE);
}

static void
handle_error_cb (GstPlayer * player, GError * error, DmpPlayer * self)
{
  DmpService *service = DMP_SERVICE (g_application_get_default ());

  g_warning ("%s", error->message);
  player_stop_decorator (self, FALSE);
  dmp_service_player_quit (service);
}

static void
handle_warning_cb (GstPlayer * player, GError * error, DmpPlayer * self)
{
  g_message ("%s", error->message);
}

static void
eos_cb (GstPlayer * player, DmpPlayer * self)
{
  g_message ("end of stream");
  player_stop_decorator (self, FALSE);
}

static gchar *
extract_title_from_uri (const gchar *uri)
{
  GFile *file;
  gchar *unescaped, *filename_for_display;

  file = g_file_new_for_uri (uri);
  unescaped = g_file_get_basename (file);
  g_object_unref (file);

  filename_for_display = g_filename_to_utf8 (unescaped, -1, NULL, NULL, NULL);
  if (!filename_for_display)  {
    filename_for_display = g_locale_to_utf8 (unescaped, -1, NULL, NULL, NULL);
    if (!filename_for_display)
      filename_for_display = g_filename_display_basename (uri);
  }
  g_free (unescaped);
  return filename_for_display;
}

static void
media_info_updated_cb (GstPlayer * player, GstPlayerMediaInfo * info,
    DmpPlayer * self)
{
  DmpService *service = DMP_SERVICE (g_application_get_default ());
  gchar *generated_title = NULL;
  DmpMediaInfo media_info = {
    .info = info,
    .uri = self->original_uri,
  };

  media_info.title = gst_player_media_info_get_title (info);
  if (!media_info.title) {
    if (self->stream_title) {
      media_info.title = self->stream_title;
    } else {
      const gchar *uri;

      uri = gst_player_media_info_get_uri (info);
      generated_title = extract_title_from_uri (uri);
      media_info.title = generated_title;
    }
  }
  toolbox_set_title (self, media_info.title);

  dmp_service_player_set_media_info (service, &media_info);
  g_free (generated_title);
}

static void
state_changed_cb (GstPlayer * player, GstPlayerState state, DmpPlayer * self)
{
  DmpService *service = DMP_SERVICE (g_application_get_default ());

  if (state == GST_PLAYER_STATE_PLAYING) {
    apply_watch_later (self);
    toolbox_start_timer (self);
  } else if (state == GST_PLAYER_STATE_PAUSED) {
    toolbox_show (self);
  }

  dmp_service_player_set_state (service, state);
}

static void
handle_seek_done_cb (GstPlayer * player, GstClockTime position,
    DmpPlayer * self)
{
  DmpService *service = DMP_SERVICE (g_application_get_default ());

  dmp_service_player_emit_seeked (service, GST_TIME_AS_USECONDS (position));
}

static void
set_position_label (GtkWidget * widget, guint64 ticks)
{
  gchar *time;
  gint h, m;
  guint64 s;

  s = GST_TIME_AS_SECONDS (ticks);
  h = s / 3600;
  s = s % 3600;
  m = s / 60;
  s = s % 60;

  if (h)
    time = g_strdup_printf ("%02d:%02d:%02ld", h, m, s);
  else
    time = g_strdup_printf ("%02d:%02ld", m, s);

  gtk_label_set_label (GTK_LABEL (widget), time);
  g_free (time);
}

static void
position_updated_cb (GstPlayer * player, GstClockTime position,
    DmpPlayer * self)
{
  GstClockTimeDiff remain;
  GstClockTime duration;
  gdouble fraction;

  duration = gst_player_get_duration (player);
  fraction = (1.0 * position) / duration;
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (self->progressbar),
      fraction);

  remain = GST_CLOCK_DIFF (position, duration);
  set_position_label (self->remain, remain);
  set_position_label (self->elapsed, position);
}

static void
dmp_player_constructed (GObject * object)
{
  DmpPlayer *self = DMP_PLAYER (object);

  create_ui (self);

  self->player = gst_player_new (self->renderer,
      gst_player_g_main_context_signal_dispatcher_new (NULL));

  configure_pipeline (self);

  gst_player_set_visualization_enabled (self->player, TRUE);

  g_signal_connect (self->player, "end-of-stream", G_CALLBACK (eos_cb), self);
  g_signal_connect (self->player, "position-updated",
      G_CALLBACK (position_updated_cb), self);
  g_signal_connect (self->player, "media-info-updated",
      G_CALLBACK (media_info_updated_cb), self);
  g_signal_connect (self->player, "state-changed",
      G_CALLBACK (state_changed_cb), self);
  g_signal_connect (self->player, "error", G_CALLBACK (handle_error_cb), self);
  g_signal_connect (self->player, "warning",
      G_CALLBACK (handle_warning_cb), self);
  g_signal_connect (self->player, "seek-done", G_CALLBACK (handle_seek_done_cb),
      self);

  g_signal_connect (self, "delete-event", G_CALLBACK (delete_event_cb), NULL);
}

static void
dmp_player_dispose (GObject * object)
{
  DmpPlayer *self = DMP_PLAYER (object);

  if (self->player)
    player_stop_decorator (self, TRUE);

  G_OBJECT_CLASS (dmp_player_parent_class)->dispose (object);
}

static void
dmp_player_finalize (GObject * object)
{
  DmpPlayer *self = DMP_PLAYER (object);

  reset_internal_state (self);

  g_clear_object (&self->player);
  g_clear_object (&self->builder);

  G_OBJECT_CLASS (dmp_player_parent_class)->finalize (object);
}

static void
dmp_player_init (DmpPlayer * self)
{
  self->requested_position = GST_CLOCK_TIME_NONE;
}

static void
dmp_player_class_init (DmpPlayerClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = dmp_player_constructed;
  object_class->dispose = dmp_player_dispose;
  object_class->finalize = dmp_player_finalize;
}

DmpPlayer *
dmp_player_new (void)
{
  return g_object_new (DMP_TYPE_PLAYER, NULL);
}

GstPlayer *
dmp_player_get_player (DmpPlayer * self)
{
  g_return_val_if_fail (self, NULL);
  return self->player;
}

void
enable_download (DmpPlayer * self, gboolean clear)
{
  GstElement *playbin;
  gint flags;

  playbin = gst_player_get_pipeline (self->player);
  if (!playbin)
    return;

  g_object_get (playbin, "flags", &flags, NULL);

  if (clear) {
    flags &= ~ 0x80;
  } else {
    flags |= 0x80;
  }

  g_object_set (playbin, "flags", flags, NULL);
  gst_object_unref (playbin);
}

void
ensure_subtitles_list (DmpPlayer * self, const gchar * real_uri)
{
  gchar *tmp = NULL;

  if (self->external_subtitles)
    return;

  if (!real_uri) {
    if (self->player)
      tmp = gst_player_get_uri (self->player);
  } else {
    tmp = (gchar *) real_uri;
  }
  if (tmp) {
    self->external_subtitles =
        find_external_subtitles (self->external_subtitles, tmp);
    if (!real_uri)
      g_free (tmp);
  }
}

void
dmp_player_open (DmpPlayer * self, const gchar * uri)
{
  gchar *real_uri, *stream_info, **info;

  real_uri = gst_uri_is_valid (uri) ? g_strdup (uri) :
    gst_filename_to_uri (uri, NULL);
  if (!real_uri)
    return;

  write_watch_later (self);
  reset_internal_state (self);
  load_watch_later (self, real_uri);

  stream_info = get_stream_info (real_uri);
  if (stream_info) {
    info = g_strsplit (stream_info, "\n", -1);
    g_free (stream_info);

    self->original_uri = real_uri;
    self->stream_title = g_strdup (info[0]);
    real_uri = g_strdup (info[1]);
    g_strfreev (info);
  }

  enable_download (self, g_str_has_suffix (real_uri, "http"));

  gst_player_set_uri (self->player, real_uri);
  ensure_subtitles_list (self, real_uri);
  gst_player_set_subtitle_track_enabled (self->player, FALSE);

  g_free (real_uri);
}

void
dmp_player_play (DmpPlayer * self)
{
  gtk_window_fullscreen (GTK_WINDOW (self));
  gtk_widget_show_all (GTK_WIDGET (self));
  gst_player_play (self->player);
}

void
dmp_player_stop (DmpPlayer * self)
{
  player_stop_decorator (self, TRUE);
}

void
dmp_player_pause (DmpPlayer * self)
{
  gst_player_pause (self->player);
}

void
dmp_player_set_position (DmpPlayer * self, GstClockTime position)
{
  GstClockTime duration, new_position;

  duration = gst_player_get_duration (self->player);
  new_position = CLAMP (position, 0, duration);

  if (GST_CLOCK_TIME_IS_VALID (new_position))
    gst_player_seek (self->player, new_position);
}

void
dmp_player_seek (DmpPlayer * self, gint64 offset)
{
  GstClockTime position, delta;
  gint64 factor, new_position;

  toolbox_show (self);

  factor = (offset < 0) ? -1 : 1;
  position = gst_player_get_position (self->player);
  delta = gst_util_uint64_scale (ABS (offset), GST_USECOND, 1);
  new_position = position + factor * delta;

  if (new_position >= 0)
    dmp_player_set_position (self, (GstClockTime) new_position);

  toolbox_start_timer (self);
}

/**
 * Sets audio volume level
 *
 * + 0.0 means mute.
 * + 1.0 is a sensible maximum volume level (ex: 0dB).
 *
 * Note that the volume may be higher than 1.0, although generally
 * clients should not attempt to set it above 1.0.
 *
 */
void
dmp_player_set_volume (DmpPlayer * self, gdouble volume)
{
  g_return_if_fail (volume >= 0 && volume <= 1);

  if (volume == 0)
    gst_player_set_mute (self->player, TRUE);
  else if (gst_player_get_mute (self->player))
    gst_player_set_mute (self->player, FALSE);
  else
    gst_player_set_volume (self->player, volume);
}

gdouble
dmp_player_get_volume (DmpPlayer * self)
{
  if (gst_player_get_mute (self->player))
    return 0;
  else
    return gst_player_get_volume (self->player);
}

static inline gchar *
set_none_subtitle (DmpPlayer * self)
{
  gchar *cur_sub;

  cur_sub = gst_player_get_subtitle_uri (self->player);
  if (cur_sub) {
    gst_player_set_subtitle_uri (self->player, NULL);
    g_free (cur_sub);
  }

  gst_player_set_subtitle_track_enabled (self->player, FALSE);
  self->subtitle_idx = 0;
  osd_set_text (self, "None");
  return g_strdup ("None");
}

static inline gboolean
set_internal_subtitle (DmpPlayer * self, GstPlayerMediaInfo * info, guint idx,
    gchar ** desc)
{
  const gchar *lang = NULL;

  if (!gst_player_set_subtitle_track (self->player, idx))
    return FALSE;
  gst_player_set_subtitle_track_enabled (self->player, TRUE);

  if (desc) {
    GList *subs = gst_player_media_info_get_subtitle_streams (info);
    if (subs) {
      GstPlayerSubtitleInfo *sinfo;

      sinfo = g_list_nth_data (subs, idx);
      if (sinfo)
        lang = gst_player_subtitle_info_get_language (sinfo);
    }
    *desc = g_strdup_printf ("internal:%u:%s", idx, lang);
    osd_set_text (self, lang);
  }

  return TRUE;
}

static inline gboolean
set_external_subtitle (DmpPlayer * self, guint idx, gchar ** desc)
{
  gchar *suburi, *loc, *name;

  suburi = g_list_nth_data (self->external_subtitles, idx);
  if (!suburi)
    return FALSE;

  gst_player_set_subtitle_uri (self->player, suburi);
  gst_player_set_subtitle_track_enabled (self->player, TRUE);

  if (desc)
    *desc = g_strdup (suburi);

  loc = g_filename_from_uri (suburi, NULL, NULL);
  if (loc) {
    name = g_path_get_basename (loc);
    osd_set_text (self, name);
    g_free (loc);
    g_free (name);
  }

  return TRUE;
}

gchar *
dmp_player_set_next_subtitle (DmpPlayer * self)
{
  GstPlayerMediaInfo *minfo;
  guint next_sub, n_int, n_ext;
  gchar *desc = NULL;

  if (!self->player)
    return g_strdup ("None");

  minfo = gst_player_get_media_info (self->player);
  if (!minfo && !self->external_subtitles)
    return set_none_subtitle (self);

  next_sub = self->subtitle_idx + 1;

  n_ext = g_list_length (self->external_subtitles);

  n_int = 0;
  if (minfo) {
    n_int = gst_player_media_info_get_number_of_subtitle_streams (minfo);

    /* XXX: if an external suburi is set,
     * gst_player_media_info_get_number_of_subtitle_streams() returns
     * +1, which is not internal */
    if (n_int > 0 && n_ext > 0)
      n_int -= 1;
  }

  if (next_sub > n_int + n_ext)
    return set_none_subtitle (self);

  if (n_int > 0 && next_sub <= n_int) {
    gboolean ret;

    ret = set_internal_subtitle (self, minfo, next_sub - 1, &desc);
    g_object_unref (minfo);
    if (!ret)
      return set_none_subtitle (self);

    self->subtitle_idx = next_sub;
    return desc;
  }

  if (minfo)
    g_object_unref (minfo);

  next_sub -= n_int;
  if (n_ext > 0 && next_sub <= n_ext) {
    if (!set_external_subtitle (self, next_sub - 1, &desc))
      return set_none_subtitle (self);

    self->subtitle_idx = next_sub + n_int;
    return desc;
  }

  return set_none_subtitle (self);
}
